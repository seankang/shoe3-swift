//
//  ViewController.swift
//  Shoe Store
//
//  Created by Pranav Wadhwa on 12/28/18.
//  Copyright © 2018 Pranav Wadhwa. All rights reserved.
//

import UIKit
import PassKit

class ViewController: UIViewController, PKPaymentAuthorizationViewControllerDelegate {

    
    
    // Data Setup
    
    struct Shoe {
        var name: String
        var price: Double
    }
    
    let shoeData = [
        Shoe(name: "Nike Air Force 1 High LV8", price: 110.00),
        Shoe(name: "adidas Ultra Boost Clima", price: 139.99),
        Shoe(name: "Jordan Retro 10", price: 190.00),
        Shoe(name: "adidas Originals Prophere", price: 49.99),
        Shoe(name: "New Balance 574 Classic", price: 90.00)
    ]
    
    // Storyboard outlets
    
    @IBOutlet weak var shoePickerView: UIPickerView!
    @IBOutlet weak var priceLabel: UILabel!
    
    
    @IBAction func buyShoeTapped(_ sender: UIButton) {
        
        let selectedIndex = shoePickerView.selectedRow(inComponent: 0)
        let shoe = shoeData[selectedIndex]
        let paymentItem = PKPaymentSummaryItem.init(label: shoe.name, amount: NSDecimalNumber(value: shoe.price))
        
        
        let paymentNetworks = [PKPaymentNetwork.amex, .discover, .masterCard, .visa]
        
        
        if PKPaymentAuthorizationViewController.canMakePayments(usingNetworks: paymentNetworks) {
            
            let request = PKPaymentRequest()
            request.currencyCode = "USD" // 1
            request.countryCode = "US" // 2
            request.merchantIdentifier = "merchant.com.seankang.shoe3" // 3
            request.merchantCapabilities = PKMerchantCapability.capability3DS // 4
            request.supportedNetworks = paymentNetworks // 5
            request.paymentSummaryItems = [paymentItem] //
            
            
             guard let paymentVC = PKPaymentAuthorizationViewController(paymentRequest: request) else {
                 displayDefaultAlert(title: "Error", message: "Unable to present Apple Pay authorization.")
                 return
             }
             print("paymentVC")
             print(paymentVC)
             paymentVC.delegate = self
             self.present(paymentVC, animated: true, completion: nil)
             

            
        } else {
            displayDefaultAlert(title: "Error", message: "Unable to make Apple Pay transaction.")
        }
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        shoePickerView.delegate = self
        shoePickerView.dataSource = self
        
    }
    
    
    func displayDefaultAlert(title: String?, message: String?) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
       let okAction = UIAlertAction(title: "OK", style: .default, handler: nil)
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func paymentAuthorizationViewControllerDidFinish(_ controller: PKPaymentAuthorizationViewController) {
        dismiss(animated: true, completion: nil)
        
    
    }
    
    func paymentAuthorizationViewController(_ controller: PKPaymentAuthorizationViewController, didAuthorizePayment payment: PKPayment, handler completion: @escaping (PKPaymentAuthorizationResult) -> Void) {
        print("token")
        print(payment.token)
        print(payment.token.paymentData)
        processPayment(payment: payment)
        print("payment")
        print(payment.self)
        dismiss(animated: true, completion: nil)
        //completion(PKPaymentAuthorizationResult)
        displayDefaultAlert(title: "Success!", message: "The Apple Pay transaction was complete.")
    }
    
    func processPayment (payment : PKPayment){
        let paymentDataDictionary: [AnyHashable: Any]? = try? JSONSerialization.jsonObject(with: payment.token.paymentData, options: .mutableContainers) as! [AnyHashable : Any]


        print(paymentDataDictionary)

        let header = paymentDataDictionary?["header"] as! [AnyHashable : Any]



        print("signature")
        let signature = paymentDataDictionary?["signature"]
        print(signature)


        print("transactionId")
        let transactionId = try? header["transactionId"]
        print(transactionId)

        print("ephemeralPublicKey")
        let ephemeralPublicKey = try? header["ephemeralPublicKey"]
        print(ephemeralPublicKey)

        print("publicKeyHash")
        let publicKeyHash = try? header["publicKeyHash"]
        print(publicKeyHash)


        print("data")
        let data = paymentDataDictionary?["data"]
        print(data)
           

        postJson {
            (status) in
                print(status)
        }
    }
    
    
    func postJson(completion: @escaping (Int) -> () ) {
        let url = URL(string: "https://stripe.com")!
        
        var request = URLRequest(url: url)
        request.httpMethod = "POST"
        let jsonBody = [
            "email": "seankang@yahoo.com"
        ]
        if let jsonBodyData = try? JSONSerialization.data(withJSONObject: jsonBody, options: []){
            URLSession.shared.uploadTask(with: request, from: jsonBodyData) {
                data, response, error in
                print("some action")
                if let httpResponse = response as? HTTPURLResponse {
                    completion(httpResponse.statusCode)
                }
            }.resume()
        }
    }
    
    
}



extension ViewController: UIPickerViewDataSource, UIPickerViewDelegate {
    
    // MARK: - Pickerview update
    
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return shoeData.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return shoeData[row].name
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let priceString = String(format: "%.02f", shoeData[row].price)
        priceLabel.text = "Price = $\(priceString)"
    }
}
